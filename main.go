package main

import (
	"fmt"
	"log"
	"net/http"

)

func FormHandler(w http.ResponseWriter, r *http.Request){
	if err:= r.ParseForm(); err!=nil{
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	fmt.Fprintf(w, "POST request successfully")
	name := r.FormValue("name")
	address := r.FormValue("address")
	fmt.Fprintf(w, "name : %s\n", name)
	fmt.Fprintf(w, "address : %s\n", address)

}

func HelloHandler(w http.ResponseWriter, r *http.Request){
	fmt.Println(r.URL)
	if r.URL.Path != "/hello" {
		http.Error(w, "404 not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not Supporting", http.StatusNotFound)
		return
	}
fmt.Fprintf(w, "hello!")
}

func main(){
	FileServer:=http.FileServer(http.Dir("./static"))
	http.Handle("/", FileServer)
	http.HandleFunc("/form" , FormHandler)
	http.HandleFunc("/hello" , HelloHandler)
	
	fmt.Printf("server8080")
	if err := http.ListenAndServe(":8080", nil); 
	err!= nil{
		log.Fatal(err)
	}
}